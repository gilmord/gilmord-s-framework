Gilmord`s framework.

The simplest variant of "framework"

Main functionality I wanted to implement is Controller system from MVC.


Route class parse url and take 3d arg as Controller name,
  4th -as controllers action-method name, and 5th - as additional param.
  Than it include needed file with Controller from url and call its action
  (also from url).

  If url arguments 3-5 missing - default controller is called.


Test urls:
  mainController/main
  mainController/second
  mainController/second/{param}
