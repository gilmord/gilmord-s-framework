<?php

require_once 'core/Model.php';
require_once 'core/View.php';
require_once 'core/Routing.php';
require_once 'core/Controller.php';

core\Routing::start();
