<?php

namespace core;

class Routing {
  static function start() {


    $controllerName = 'mainController';
    $actionName = 'mainAction';

    $args = self::arg();

    if (!empty($args[2])) {
      $controllerName = $args[2];
    }
    if (!empty($args[3])) {
      $actionName = "{$args[3]}Action";
    }
    $modelName = "Model_{$controllerName}";
    $fileWithModel = "{strtolower($modelName)}.php";
    $fileWithModelPath = "app/models/{$fileWithModel}";

    if (file_exists($fileWithModelPath)) {
      include $fileWithModelPath;
    }
    $fileWithController = "{$controllerName}.php";
    $fileWithControllerPath = "app/controllers/" . $fileWithController;

    if (file_exists($fileWithControllerPath)) {
      include $fileWithControllerPath;
    }
    else {
      self::setExeption('file', $fileWithControllerPath);
    }
    $controllerName = "controllers\\{$controllerName}";
    $controller = new $controllerName;
    $action = $actionName;

    if (method_exists($controller, $action)) {
      if ( isset($args[4]) ) {
        call_user_func(array($controller, $actionName), $args[4]);
      }
      else {
        call_user_func(array($controller, $actionName));
      }
    }
    else {
      self::setExeption('action', $action);
    }
  }

  static function setExeption($type, $param) {
    if ($type == 'action') {
      $message = "Action '{$param}' not found";
      exit($message);
    }
    if ($type == 'file') {
      $message = "File '{$param}' not found";
      exit($message);
    }
  }

  static function arg() {
    return explode('/', $_SERVER['REQUEST_URI']);
  }
}
