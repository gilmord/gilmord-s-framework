<?php

namespace core;

class Controller {

  public $model;
  public $view;

  function __construct() {
    $this->view = new View();
  }

  function indexAction() {
    $this->view->render('template.php');
  }
}
