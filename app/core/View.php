<?php

namespace core;

class View {
  function render($template = 'template') {
    require_once 'app/templates/' . $template;
  }
}
