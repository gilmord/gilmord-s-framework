<?php

namespace controllers;

use core\Controller;

class mainController extends Controller {
  function mainAction() {
    $this->view->render('template.php');
  }

  function secondAction($data = NULL) {
    if ($data) {
      $this->view->render('template_with_data.php');
    }
    else {
      $this->view->render('template.php');
    }
  }
}
